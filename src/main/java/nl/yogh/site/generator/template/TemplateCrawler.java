package nl.yogh.site.generator.template;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

import nl.yogh.site.generator.content.ContentRepository;
import nl.yogh.site.generator.content.CopyAction;
import nl.yogh.site.generator.files.CopyDir;

public class TemplateCrawler {
  private static final String DIRECTORY_TEMPLATES = "_templates";
  private static final String DIRECTORY_TASKS = "_tasks";

  private static final Logger LOG = LoggerFactory.getLogger(TemplateCrawler.class);

  private final ContentRepository contentRepo;

  public TemplateCrawler(final ContentRepository contentRepo) {
    this.contentRepo = contentRepo;
  }

  public static void generateSite(final Path templateRoot, final ContentRepository contentRepo, final Path target) throws IOException {
    new TemplateCrawler(contentRepo).generate(templateRoot, target);
  }

  public void generate(final Path root, final Path target) throws IOException {
    LOG.info("Filtering content.");
    // Step 1, filter content with tasks
    final Path tasks = root.resolve(DIRECTORY_TASKS);
    if (Files.exists(root.resolve(DIRECTORY_TASKS))) {
      Files.list(tasks).forEach(this::filterPath);
    }
    LOG.info("Filtered:\n{}", contentRepo);

    LOG.info("Generating base site.");
    // Step 2, crawl the root and generate base site
    Files.list(root).forEach(consumePath(target));
    LOG.info("Done.");

    LOG.info("Generating sitemap.");
    // Step 3, crawl the content and generate sitemap
    contentRepo.forEach(Exceptions.sneak().fromBiConsumer((k, v) -> generateTemplateRoot(root, target.resolve(k), k, v)));
    LOG.info("Done.");

    LOG.info("Copying assets.");
    // Step 4, copy content assets to target
    contentRepo.getCopyActions().forEach(Exceptions.sneak().consumer(v -> performCopyAction(target, v)));
    LOG.info("Done.");
  }

  private void filterPath(final Path task) {
    LOG.debug("Filtering {}", task);

    final Map<String, Object> filter = LiquidGenerator.filter(contentRepo, task);
  }

  private void performCopyAction(final Path targetRoot, final CopyAction action) throws IOException {
    final Path target = targetRoot.resolve(action.getRelativized());

    LOG.debug("Copying from {} to {}", action.getSource(), target);

    Files.createDirectories(target.getParent());
    Files.copy(action.getSource(), target, StandardCopyOption.REPLACE_EXISTING);
  }

  @SuppressWarnings("unchecked")
  private void generateTemplateRoot(final Path templateRoot, final Path target, final String key, final Object value) throws IOException {
    LOG.debug("Generating: {} into {}", key, target);

    if (value instanceof List) {
      LOG.debug("Content: {}", value);
      generateTemplatedCollection(templateRoot.resolve(DIRECTORY_TEMPLATES), target, key, (List<Object>) value);
    } else if (value.getClass().isArray()) {
      generateTemplatedCollection(templateRoot.resolve(DIRECTORY_TEMPLATES), target, key, Arrays.asList((Object[]) value));
    } else {
      // No action
    }
  }

  private void generateTemplatedCollection(final Path templateRoot, final Path target, final String key, final List<Object> values)
      throws IOException {
    values.forEach(Exceptions.sneak().consumer(v -> generateTemplatedItem(templateRoot, target, key, v)));
  }

  @SuppressWarnings("unchecked")
  private void generateTemplatedItem(final Path templateRoot, final Path target, final String key, final Object content) throws IOException {
    if (content instanceof Map) {
      generateTemplatedItem(templateRoot, target, key, (Map<String, Object>) content);
    } else if (content instanceof String) {
      final Map<String, Object> map = new HashMap<>();
      map.put("value", content);
      map.put("name", content);
      generateTemplatedItem(templateRoot, target, key, map);
    }
  }

  private void generateTemplatedItem(final Path templateRoot, final Path target, final String key, final Map<String, Object> content)
      throws IOException {
    Files.createDirectories(target);
    final String layout = findLayout(key, content);

    content.put("_root", contentRepo);

    LOG.debug("Generating layout [{}] for {}", layout, content);
    final Writer out = new BufferedWriter(new FileWriter(target.toString() + "/" + content.get("name") + ".html"));
    LiquidGenerator.generate(content, templateRoot.resolve(layout), out);
    out.close();
  }

  private String findLayout(final String key, final Map<String, Object> content) {
    final String layout;

    if (content.containsKey("layout")) {
      layout = (String) content.get("layout");
    } else if (key.endsWith("s")) {
      layout = key.substring(0, key.length() - 1);
    } else {
      layout = key;
    }

    return layout + ".html";
  }

  private Consumer<? super Path> consumePath(final Path target) {
    return Exceptions.sneak().consumer(p -> consumeTemplatePath(p, target));
  }

  private void consumeTemplatePath(final Path p, final Path target) throws IOException {
    if (isIgnored(p) || isLiquidBase(p)) {
      // Do nothing.
    } else if (isLiquidParseable(p)) {
      parse(p, target);
    } else {
      copy(p, target);
    }
  }

  private boolean isLiquidParseable(final Path p) {
    return p.getFileName().toString().endsWith(".html");
  }

  private void parse(final Path path, final Path target) throws IOException {
    LOG.debug("Path: {}", path);

    final Writer out = new BufferedWriter(new FileWriter(target.toString() + "/" + path.getFileName().toString()));
    LiquidGenerator.generate(contentRepo, path, out);
    out.close();
  }

  private static boolean isIgnored(final Path p) {
    return p.getFileName().toString().startsWith(".");
  }

  private static boolean isLiquidBase(final Path p) {
    return p.getFileName().toString().startsWith("_");
  }

  private static void copy(final Path v, final Path target) throws IOException {
    Files.walkFileTree(v, new CopyDir(v, target.resolve(v.getFileName())));
  }
}
