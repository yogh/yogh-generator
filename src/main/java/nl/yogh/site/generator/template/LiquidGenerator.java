package nl.yogh.site.generator.template;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liqp.ParseSettings;
import liqp.Template;
import liqp.parser.Flavor;

public class LiquidGenerator {
  private static final Logger LOG = LoggerFactory.getLogger(LiquidGenerator.class);

  public static Map<String, Object> filter(final Map<String, Object> keys, final Path path) {
    try {
      keys.putAll(buildTemplate(path).filter(keys));
      return keys;
    } catch (final IOException e) {
      throw new RuntimeException("Problem while filtering template.", e);
    }
  }

  public static void generate(final Map<String, Object> keys, final Path path, final Writer out) {
    try {
      final String rendered = buildTemplate(path).render(keys);
      out.write(rendered);

      if (LOG.isDebugEnabled()) {
        LOG.debug("Result: {}", rendered.replaceAll("\t", "").replaceAll("\n", ""));
      }
    } catch (final IOException e) {
      throw new RuntimeException("Problem while rendering template.", e);
    }
  }

  private static Template buildTemplate(final Path path) throws IOException {
    final ParseSettings settings = new ParseSettings.Builder().withFlavor(Flavor.JEKYLL).build();

    return Template.parse(path.toFile(), settings);
  }
}
