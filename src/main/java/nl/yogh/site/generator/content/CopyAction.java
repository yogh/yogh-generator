package nl.yogh.site.generator.content;

import java.nio.file.Path;

public class CopyAction {

  private final Path source;
  private final Path relativized;

  public CopyAction(final Path source, final Path relativized) {
    this.source = source;
    this.relativized = relativized;
  }

  public Path getSource() {
    return source;
  }

  public Path getRelativized() {
    return relativized;
  }
}
