package nl.yogh.site.generator.content;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.commonmark.Extension;
import org.commonmark.ext.front.matter.YamlFrontMatterExtension;
import org.commonmark.ext.front.matter.YamlFrontMatterVisitor;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class MarkdownGenerator {
  private static final int WORD_SIZE = 6;
  private static final int PREVIEW_WORDS_LENGTH = 150;

  private static final Logger LOG = LoggerFactory.getLogger(MarkdownGenerator.class);

  private MarkdownGenerator() {}

  public static void generate(final Path path, final Map<String, Object> map) throws IOException {
    final List<Extension> extensions = Arrays.asList(YamlFrontMatterExtension.create());

    final Parser parser = Parser.builder().extensions(extensions).build();
    Node document;
    try {
      document = parser.parseReader(new FileReader(path.toFile()));
    } catch (final FileNotFoundException e) {
      throw new RuntimeException("FileNotFoundException that should not be able to happen.");
    }

    final YamlFrontMatterVisitor frontMatter = new YamlFrontMatterVisitor();
    document.accept(frontMatter);

    frontMatter.getData().forEach((k, v) -> map.put(k, objectFilter(v.get(0))));

    final String filename = path.getFileName().toString();
    map.put("name", filename.substring(0, filename.lastIndexOf(".")));

    final HtmlRenderer renderer = HtmlRenderer.builder().build();
    final String generatedContent = renderer.render(document);

    if (!generatedContent.isEmpty()) {
      map.put("content", generatedContent);

      if (!map.containsKey("snippet")) {
        map.put("snippet", createPreview(generatedContent));
      }
    }
  }

  private static Object objectFilter(final String string) {
    if (string.startsWith("[") && string.endsWith("]")) {
      final String stripped = string.substring(1, string.length() - 1);
      return new ArrayList<String>(Arrays.asList(stripped.split(",")));
    }

    return string;
  }

  private static String createPreview(final String generatedContent) {
    final int index = generatedContent.indexOf(" ", WORD_SIZE * PREVIEW_WORDS_LENGTH);
    if (index == -1) {
      return generatedContent;
    }

    final StringBuilder bldr = new StringBuilder();
    bldr.append(generatedContent.substring(0, index));
    bldr.append("...");

    return bldr.toString();
  }
}
