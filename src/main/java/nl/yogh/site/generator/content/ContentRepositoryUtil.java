package nl.yogh.site.generator.content;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ContentRepositoryUtil {
  private static final Logger LOG = LoggerFactory.getLogger(ContentRepositoryUtil.class);

  private ContentRepositoryUtil() {}

  static void stringifyMap(final Map<String, Object> map, final StringBuilder bldr) {
    stringifyMap(map, bldr, 0);
  }

  private static void stringifyMap(final Map<String, Object> map, final StringBuilder bldr, final int depth) {
    map.entrySet().forEach(v -> {
      stringifyEntry(v, bldr, depth);
      newLine(bldr, depth);
    });
  }

  @SuppressWarnings("unchecked")
  private static void stringifyObject(final Object object, final StringBuilder bldr, final int depth) {
    if (object == null) {
      return;
    }

    if (object instanceof String) {
      stringifyString((String) object, bldr, depth);
    } else if (object instanceof Map) {
      newLine(bldr, depth);
      stringifyMap((Map<String, Object>) object, bldr, depth + 1);
    } else if (object instanceof List) {
      newLine(bldr, depth);
      stringifyList((List<Object>) object, bldr, depth + 1);
    } else if (object.getClass().isArray()) {
      newLine(bldr, depth);
      stringifyArray((Object[]) object, bldr, depth + 1);
    } else if (object instanceof Entry) {
      stringifyEntry((Entry<String, Object>) object, bldr, depth);
    } else {
      LOG.error("Cannot parse object of type {}", object.getClass().getSimpleName());
    }
  }

  private static void stringifyEntry(final Entry<String, Object> entry, final StringBuilder bldr, final int depth) {
    newIndent(bldr, depth);
    bldr.append(entry.getKey());
    bldr.append(objectTypeNotation(entry.getValue()));
    bldr.append(": ");

    stringifyObject(entry.getValue(), bldr, depth);
  }

  private static String objectTypeNotation(final Object value) {
    if (value instanceof List) {
      return " []";
    } else if (value instanceof Map) {
      return " {}";
    }

    return "";
  }

  private static void stringifyList(final List<Object> list, final StringBuilder bldr, final int depth) {
    int counter = 0;
    for (final Object item : list) {
      newIndent(bldr, depth);
      bldr.append(counter);
      bldr.append(": ");
      counter++;
      stringifyObject(item, bldr, depth);
      newLine(bldr, depth);
    }
  }

  private static void stringifyArray(final Object[] list, final StringBuilder bldr, final int depth) {
    int counter = 0;
    for (final Object item : list) {
      newIndent(bldr, depth);
      bldr.append(counter);
      bldr.append(": ");
      counter++;
      stringifyObject(item, bldr, depth);
      newLine(bldr, depth);
    }
  }

  private static void stringifyString(final String key, final String value, final StringBuilder bldr, final int tabs) {
    stringifyString(key + ": " + value, bldr, tabs);
  }

  private static void stringifyString(final String value, final StringBuilder bldr, final int tabs) {
    bldr.append(stripString(value));
  }

  private static void newLine(final StringBuilder bldr, final int tabs) {
    bldr.append("\n");
  }

  private static void newIndent(final StringBuilder bldr, final int tabs) {
    bldr.append(repeatedTabs(tabs));
  }

  private static String stripString(final String value) {
    return value.replaceAll("\t", "").replaceAll("\n", "");
  }

  private static String repeatedTabs(final int count) {
    final StringBuilder r = new StringBuilder();
    for (int i = 0; i < count; i++) {
      r.append("  ");
    }
    return r.toString();
  }
}
