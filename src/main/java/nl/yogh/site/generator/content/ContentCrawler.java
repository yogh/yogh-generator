package nl.yogh.site.generator.content;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

public class ContentCrawler {
  private static final Logger LOG = LoggerFactory.getLogger(ContentCrawler.class);

  public static ContentRepository scavenge(final Path content) {
    final ContentRepository repo = new ContentRepository();

    // Traverse the content
    LOG.debug("");
    LOG.debug("Consuming root dir {}", content);
    try {
      consumeContentPath(content, repo);
    } catch (final IOException e) {
      // throw e;
      LOG.error("Error while consuming content.", e);
    }

    return repo;
  }

  private static void consumeContentPath(final Path target, final ContentRepository repo) throws IOException {
    consumeContentPath(target, target, repo);
  }

  private static void consumeContentPath(final Path root, final Path content, final ContentRepository repo) throws IOException {
    final String fileName = content.getFileName().toString();

    if (fileName.startsWith(".")) {
      LOG.debug("Skipping. (hidden)");
      return;
    } else if (Files.isDirectory(content)) {
      Files.list(content).forEach(Exceptions.sneak().consumer(v -> consumeContentPath(root, v, repo)));
    } else {
      consumeContentItem(root, content, repo);
    }
  }

  private static void consumeContentItem(final Path root, final Path content, final ContentRepository repo) {
    final Path directory = content.getParent().getFileName();

    if (directory.toString().startsWith("_")) {
      final String key = directory.getFileName().toString().substring(1);
      if (!repo.containsKey(key)) {
        repo.put(key, new ArrayList<String>());
      }

      @SuppressWarnings("unchecked")
      final List<Map<String, Object>> lst = (List<Map<String, Object>>) repo.get(key);

      final Map<String, Object> map = new HashMap<>();

      Exceptions.sneak().run(() -> MarkdownGenerator.generate(content, map));

      lst.add(map);
    } else {

      repo.registerCopyTask(root, content);
    }
  }
}
