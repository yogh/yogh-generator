package nl.yogh.site.generator.content;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContentRepository extends HashMap<String, Object> {
  private static final long serialVersionUID = -2253930572203162385L;

  private final List<CopyAction> copyActions = new ArrayList<>();

  public void registerCopyTask(final Path root, final Path content) {
    final Path relativized = root.relativize(content);
    final CopyAction action = new CopyAction(content, relativized);

    getCopyActions().add(action);
  }

  public List<CopyAction> getCopyActions() {
    return copyActions;
  }

  @Override
  public String toString() {
    final StringBuilder bldr = new StringBuilder();

    ContentRepositoryUtil.stringifyMap(this, bldr);

    return bldr.toString();
  }
}
