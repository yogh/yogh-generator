package nl.yogh.site.generator.files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteDir extends SimpleFileVisitor<Path> {
  private final Path root;

  public DeleteDir(final Path root) {
    this.root = root;
  }

  @Override
  public FileVisitResult visitFile(final Path file, final BasicFileAttributes attributes) throws IOException {
    Files.delete(file);
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult postVisitDirectory(final Path directory, final IOException exception) throws IOException {
    if (exception == null) {
      if (directory.equals(root)) {
        return FileVisitResult.TERMINATE;
      }

      Files.delete(directory);
      return FileVisitResult.CONTINUE;
    } else {
      throw exception;
    }
  }
}
