package nl.yogh.site.generator.files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyDir extends SimpleFileVisitor<Path> {
  private static final Logger LOG = LoggerFactory.getLogger(CopyDir.class);

  private final Path sourceDir;
  private final Path targetDir;

  public CopyDir(final Path sourceDir, final Path targetDir) {
    this.sourceDir = sourceDir;
    this.targetDir = targetDir;
  }

  @Override
  public FileVisitResult visitFile(final Path file, final BasicFileAttributes attributes) throws IOException {
    try {
      final Path targetFile = targetDir.resolve(sourceDir.relativize(file));
      Files.copy(file, targetFile, StandardCopyOption.REPLACE_EXISTING);
    } catch (final IOException e) {
      LOG.error("Could not create directory.", file.getFileName(), e);
      throw e;
    }

    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attributes) throws IOException {
    try {
      final Path newDir = targetDir.resolve(sourceDir.relativize(dir));
      if (!Files.exists(newDir)) {
        Files.createDirectory(newDir);
      }
    } catch (final IOException e) {
      LOG.error("Could not create directory. {}", dir.getFileName(), e);
      throw e;
    }

    return FileVisitResult.CONTINUE;
  }
}
