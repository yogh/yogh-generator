package nl.yogh.site.generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liqp.parser.Flavor;
import nl.yogh.site.config.ApplicationConfiguration;
import nl.yogh.site.generator.content.ContentCrawler;
import nl.yogh.site.generator.content.ContentRepository;
import nl.yogh.site.generator.template.TemplateCrawler;
import nl.yogh.site.liqp.LiquidUtils;

public class SiteGenerator {
  private static final Logger LOG = LoggerFactory.getLogger(SiteGenerator.class);

  private final ApplicationConfiguration cfg;

  public SiteGenerator(final ApplicationConfiguration cfg) {
    this.cfg = cfg;

    Flavor.LIQUID.setSnippetsRoot(cfg.getSiteTemplateLocation());
    Flavor.JEKYLL.setSnippetsRoot(cfg.getSiteTemplateLocation());

    LiquidUtils.init();
  }

  public static SiteGenerator create(final ApplicationConfiguration cfg) {
    return new SiteGenerator(cfg);
  }

  public void build() throws IOException {
    build(cfg.getSiteContentLocation(), cfg.getSiteTemplateLocation(), cfg.getSiteGenerateTarget());
  }

  private void build(final String content, final String template, final String target) throws IOException {
    build(Paths.get(content), Paths.get(template), Paths.get(target));
  }

  private void build(final Path content, final Path template, final Path target) throws IOException {
	Files.createDirectories(target);
	  
    LOG.info("Crawling for content..");
    final ContentRepository contentRepo = ContentCrawler.scavenge(content);
    LOG.info("Done.");

    // Traverse the template
    LOG.debug("");
    LOG.debug("Generating site via templates: {}", template);
    TemplateCrawler.generateSite(template, contentRepo, target);
  }
}
