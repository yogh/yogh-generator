package nl.yogh.site.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationLoader {
  private static final Logger LOG = LoggerFactory.getLogger(ConfigurationLoader.class);

  private static final String CONFIG_PROPERTIES_NAME = "config/site.conf";

  private static ApplicationConfiguration cachedCfg;

  public static Properties findProperties(final String file) {
    final Properties props = new Properties();

    final int propNum = props.size();

    String search;
    if (file == null) {
      LOG.info("No config file specified. Looking for default file ({}) in classpath.", CONFIG_PROPERTIES_NAME);
      search = CONFIG_PROPERTIES_NAME;
    } else {
      LOG.info("Config file specified. Looking for config file at ({}).", file);
      search = file;
    }

    try {
      props.load(Files.newBufferedReader(Paths.get(search)));
    } catch (final IOException e) {
      LOG.error("IOException while trying to load configuration.", e);
      throw new RuntimeException("Configuration could not be loaded.", e);
    }

    LOG.info("Loaded configuration ({} variables) from file: {}", props.size() - propNum,
        search);

    return props;
  }

  public static ApplicationConfiguration generate(final String file) {
    final ApplicationConfiguration cfg = new ApplicationConfiguration(findProperties(file));

    ConfigurationLoader.cachedCfg = cfg;

    return cfg;
  }

  public static ApplicationConfiguration getCachedInstance() {
    return cachedCfg;
  }
}
