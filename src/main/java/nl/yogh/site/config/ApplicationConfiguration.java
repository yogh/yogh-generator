package nl.yogh.site.config;

import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationConfiguration {
  private static final Logger LOG = LoggerFactory.getLogger(ApplicationConfiguration.class);

  private static final String SITE_CONTENT_LOCATION = "site.content.location";
  private static final String SITE_TEMPLATE_LOCATION = "site.template.location";
  private static final String SITE_GENERATE_TARGET = "site.generate.target";

  private final Properties properties;

  public ApplicationConfiguration(final Properties properties) {
    this.properties = properties;

    validate();
  }

  public String getSiteContentLocation() {
    return getPropertyRequired(properties, SITE_CONTENT_LOCATION);
  }

  public String getSiteTemplateLocation() {
    return getPropertyRequired(properties, SITE_TEMPLATE_LOCATION);
  }

  public String getSiteGenerateTarget() {
    return getPropertyRequired(properties, SITE_GENERATE_TARGET);
  }

  private static String getPropertyRequired(final Properties properties, final String key) {
    return getProperty(properties, key).orElseThrow(() -> new IllegalStateException("Required property not present: " + key));
  }

  private static Optional<String> getProperty(final Properties properties, final String key) {
    return Optional.of(properties.getProperty(key));
  }

  private void validate() {
    validatePresence(properties, SITE_CONTENT_LOCATION);
    validatePresence(properties, SITE_TEMPLATE_LOCATION);
  }

  private static void validatePresence(final Properties properties, final String key) {
    if (!properties.containsKey(key)) {
      LOG.error("Configuration not complete. Missing element: [{}]", key);
      throw new IllegalStateException("Configuration not complete. Missing element: " + key);
    }
  }
}
