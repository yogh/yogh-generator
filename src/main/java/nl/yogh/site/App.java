package nl.yogh.site;

public class App {
  public static void main(final String[] args) {
    ApplicationFactory.init(args.length > 0 ? args[0] : null);
  }
}
