package nl.yogh.site;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.yogh.site.config.ApplicationConfiguration;
import nl.yogh.site.config.ConfigurationLoader;
import nl.yogh.site.generator.SiteGenerator;

public class ApplicationFactory {
  private static final Logger LOG = LoggerFactory.getLogger(ApplicationFactory.class);

  public static void init(final String configFile) {
    final ApplicationConfiguration cfg = ConfigurationLoader.generate(configFile);

    LOG.info("Application initialized.");

    try {
      SiteGenerator.create(cfg).build();

      LOG.info("Finished building website.");

    } catch (final IOException e) {
      LOG.error("IOException while generating the website.", e);
      throw new RuntimeException(e);
    }
  }
}
