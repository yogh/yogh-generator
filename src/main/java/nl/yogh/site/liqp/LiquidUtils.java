package nl.yogh.site.liqp;

import liqp.filters.Array;
import liqp.filters.Filter;
import liqp.tags.Tag;

public final class LiquidUtils {
  private LiquidUtils() {}

  public static void init() {
    Tag.registerTag(new DebugTag());
    Filter.registerFilter(new Array());
  }
}
