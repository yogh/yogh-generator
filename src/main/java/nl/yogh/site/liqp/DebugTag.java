package nl.yogh.site.liqp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liqp.TemplateContext;
import liqp.nodes.LNode;
import liqp.tags.Tag;

public class DebugTag extends Tag {
  private static final Logger LOG = LoggerFactory.getLogger(DebugTag.class);

  public DebugTag() {
    super("debug");
  }

  @Override
  public Object render(final TemplateContext context, final LNode... nodes) {
    final String output = super.asString(nodes[0].render(context));

    // Capture causes variable to be saved "globally"
    LOG.info("{}", output);

    return "";
  }
}
