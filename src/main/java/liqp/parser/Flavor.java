package liqp.parser;

public enum Flavor {

  LIQUID("snippets"), JEKYLL("_includes");

  private String relativeSnippetsFolderName;

  public String snippetsFolderName;

  Flavor(final String relativeSnippetsFolderName) {
    this.relativeSnippetsFolderName = relativeSnippetsFolderName;
  }

  public void setSnippetsRoot(final String root) {
    this.snippetsFolderName = root + "/" + relativeSnippetsFolderName;
  }
}
