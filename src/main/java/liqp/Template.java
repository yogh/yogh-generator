package liqp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ParseTree;

import com.fasterxml.jackson.databind.ObjectMapper;

import liqp.filters.Filter;
import liqp.nodes.LNode;
import liqp.parser.Flavor;
import liqp.parser.v4.NodeVisitor;
import liqp.tags.Tag;
import liquid.parser.v4.LiquidLexer;
import liquid.parser.v4.LiquidParser;

/**
 * The main class of this library. Use one of its static <code>parse(...)</code> to get a hold of a reference.
 * <p/>
 * Also see: https://github.com/Shopify/liquid
 */
public class Template {

  /**
   * The root of the parse tree denoting the Liquid input source.
   */
  private final ParseTree root;

  /**
   * This instance's tags.
   */
  private final Map<String, Tag> tags;

  /**
   * This instance's filters.
   */
  private final Map<String, Filter> filters;

  private final long templateSize;

  private ProtectionSettings protectionSettings = new ProtectionSettings.Builder().build();

  private RenderSettings renderSettings = new RenderSettings.Builder().build();

  private final ParseSettings parseSettings;

  /**
   * Creates a new Template instance from a given input.
   *
   * @param input
   *          the file holding the Liquid source.
   * @param tags
   *          the tags this instance will make use of.
   * @param filters
   *          the filters this instance will make use of.
   */
  private Template(final String input, final Map<String, Tag> tags, final Map<String, Filter> filters, final ParseSettings parseSettings) {

    this.tags = tags;
    this.filters = filters;
    this.parseSettings = parseSettings;

    final CharStream stream = CharStreams.fromString(input);
    this.templateSize = stream.size();
    final LiquidLexer lexer = new LiquidLexer(stream, parseSettings.stripSpacesAroundTags, parseSettings.stripSingleLine);
    try {
      root = parse(lexer);
    } catch (final Exception e) {
      throw new RuntimeException("could not parse input: " + input, e);
    }
  }

  /**
   * Creates a new Template instance from a given file.
   *
   * @param file
   *          the file holding the Liquid source.
   */
  private Template(final File file, final Map<String, Tag> tags, final Map<String, Filter> filters, final ParseSettings parseSettings)
      throws IOException {

    this.tags = tags;
    this.filters = filters;
    this.parseSettings = parseSettings;
    final CharStream stream = CharStreams.fromFileName(file.getAbsolutePath());

    try {
      this.templateSize = stream.size();
      final LiquidLexer lexer = new LiquidLexer(stream, parseSettings.stripSpacesAroundTags, parseSettings.stripSingleLine);
      root = parse(lexer);
    } catch (final Exception e) {
      throw new RuntimeException("could not parse input from " + file, e);
    }
  }

  private ParseTree parse(final LiquidLexer lexer) {

    lexer.removeErrorListeners();

    lexer.addErrorListener(new BaseErrorListener() {
      @Override
      public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line, final int charPositionInLine,
          final String msg, final RecognitionException e) {
        throw new RuntimeException(String.format("lexer error on line %s, index %s", line, charPositionInLine), e);
      }
    });

    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    final LiquidParser parser = new LiquidParser(tokens);

    parser.removeErrorListeners();

    parser.addErrorListener(new BaseErrorListener() {
      @Override
      public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line, final int charPositionInLine,
          final String msg, final RecognitionException e) {
        throw new RuntimeException(String.format("parser error on line %s, index %s", line, charPositionInLine), e);
      }
    });

    parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
    try {
      return parser.parse();
    } catch (final Exception e) {
      tokens.seek(0);
      parser.reset();
      parser.getInterpreter().setPredictionMode(PredictionMode.LL);
      return parser.parse();
    }
  }

  /**
   * Returns the root of the parse tree of the parsed input.
   *
   * @return the root of the parse tree of the parsed input.
   */
  public ParseTree getParseTree() {
    return root;
  }

  /**
   * Returns a new Template instance from a given input string.
   *
   * @param input
   *          the input string holding the Liquid source.
   *
   * @return a new Template instance from a given input string.
   */
  public static Template parse(final String input) {
    return new Template(input, Tag.getTags(), Filter.getFilters(), new ParseSettings.Builder().build());
  }

  /**
   * Returns a new Template instance from a given input file.
   *
   * @param file
   *          the input file holding the Liquid source.
   *
   * @return a new Template instance from a given input file.
   */
  public static Template parse(final File file) throws IOException {
    return new Template(file, Tag.getTags(), Filter.getFilters(), new ParseSettings.Builder().build());
  }

  public static Template parse(final File file, final ParseSettings settings) throws IOException {
    return new Template(file, Tag.getTags(), Filter.getFilters(), settings);
  }

  public static Template parse(final String input, final ParseSettings settings) {
    return new Template(input, Tag.getTags(), Filter.getFilters(), settings);
  }

  @Deprecated // Use `parse(file, settings)` instead
  public static Template parse(final File file, final Flavor flavor) throws IOException {
    final ParseSettings settings = new ParseSettings.Builder().withFlavor(flavor).build();
    return parse(file, settings);
  }

  @Deprecated // Use `parse(input, settings)` instead
  public static Template parse(final String input, final Flavor flavor) throws IOException {
    final ParseSettings settings = new ParseSettings.Builder().withFlavor(flavor).build();
    return parse(input, settings);
  }

  public Template with(final Tag tag) {
    this.tags.put(tag.name, tag);
    return this;
  }

  public Template with(final Filter filter) {
    this.filters.put(filter.name, filter);
    return this;
  }

  public Template withProtectionSettings(final ProtectionSettings protectionSettings) {
    this.protectionSettings = protectionSettings;
    return this;
  }

  public Template withRenderSettings(final RenderSettings renderSettings) {
    this.renderSettings = renderSettings;
    return this;
  }

  /**
   * Renders the template.
   *
   * @param jsonMap
   *          a JSON-map denoting the (possibly nested) variables that can be used in this Template.
   *
   * @return a string denoting the rendered template.
   */
  @SuppressWarnings("unchecked")
  public String render(final String jsonMap) {

    Map<String, Object> map;

    try {
      map = new ObjectMapper().readValue(jsonMap, HashMap.class);
    } catch (final Exception e) {
      throw new RuntimeException("invalid json map: '" + jsonMap + "'", e);
    }

    return render(map);
  }

  public String render() {
    return render(new HashMap<String, Object>());
  }

  /**
   * Renders the template.
   *
   * @param key
   *          a key
   * @param value
   *          the value belonging to the key
   * @param keyValues
   *          an array denoting key-value pairs where the uneven numbers (even indexes) should be Strings. If the length of this array is uneven, the
   *          last key (without the value) gets `null` attached to it. Note that a call to this method with a single String as parameter, will be
   *          handled by `render(String jsonMap)` instead.
   *
   * @return a string denoting the rendered template.
   */
  public String render(final String key, final Object value, final Object... keyValues) {
    return render(false, key, value, keyValues);
  }

  public String render(final boolean convertValueToMap, String key, Object value, final Object... keyValues) {

    final Map<String, Object> map = new HashMap<String, Object>();
    putStringKey(convertValueToMap, key, value, map);

    for (int i = 0; i < keyValues.length - 1; i += 2) {
      key = String.valueOf(keyValues[i]);
      value = keyValues[i + 1];
      putStringKey(convertValueToMap, key, value, map);
    }

    return render(map);
  }

  /**
   * Renders the template.
   *
   * @param variables
   *          a Map denoting the (possibly nested) variables that can be used in this Template.
   *
   * @return a string denoting the rendered template.
   */
  public String render(final Map<String, Object> variables) {
    return render(variables, Executors.newSingleThreadExecutor(), true);
  }

  public String render(final Map<String, Object> variables, final ExecutorService executorService, final boolean shutdown) {

    if (this.templateSize > this.protectionSettings.maxTemplateSizeBytes) {
      throw new RuntimeException("template exceeds " + this.protectionSettings.maxTemplateSizeBytes + " bytes");
    }

    final NodeVisitor visitor = new NodeVisitor(this.tags, this.filters, this.parseSettings);

    final Callable<String> task = () -> {
      try {
        final LNode node = visitor.visit(root);
        final Object rendered = node.render(new TemplateContext(protectionSettings, renderSettings, parseSettings.flavor, variables));
        return rendered == null ? "" : String.valueOf(rendered);
      } catch (final Exception e) {
        throw new RuntimeException(e);
      }
    };

    try {
      final Future<String> future = executorService.submit(task);
      return future.get(this.protectionSettings.maxRenderTimeMillis, TimeUnit.MILLISECONDS);
    } catch (final TimeoutException e) {
      throw new RuntimeException("exceeded the max amount of time (" +
          this.protectionSettings.maxRenderTimeMillis + " ms.)");
    } catch (final Throwable t) {
      throw new RuntimeException("Oops, something unexpected happened: ", t);
    } finally {
      if (shutdown) {
        executorService.shutdown();
      }
    }
  }

  /**
   * Renders the template.
   *
   * @param variables
   *          a Map denoting the (possibly nested) variables that can be used in this Template.
   *
   * @return a string denoting the rendered template.
   */
  public Map<String, Object> filter(final Map<String, Object> variables) {
    return filter(variables, Executors.newSingleThreadExecutor(), true);
  }

  public Map<String, Object> filter(final Map<String, Object> variables, final ExecutorService executorService, final boolean shutdown) {

    if (this.templateSize > this.protectionSettings.maxTemplateSizeBytes) {
      throw new RuntimeException("template exceeds " + this.protectionSettings.maxTemplateSizeBytes + " bytes");
    }

    final NodeVisitor visitor = new NodeVisitor(this.tags, this.filters, this.parseSettings);

    final TemplateContext templateContext = new TemplateContext(protectionSettings, renderSettings, parseSettings.flavor, variables);

    final Callable<String> task = () -> {
      try {
        final LNode node = visitor.visit(root);
        final Object rendered = node.render(templateContext);
        return rendered == null ? "" : String.valueOf(rendered);
      } catch (final Exception e) {
        throw new RuntimeException(e);
      }
    };

    try {
      final Future<String> future = executorService.submit(task);
      future.get(this.protectionSettings.maxRenderTimeMillis, TimeUnit.MILLISECONDS);
      return templateContext.getVariables();
    } catch (final TimeoutException e) {
      throw new RuntimeException("exceeded the max amount of time (" +
          this.protectionSettings.maxRenderTimeMillis + " ms.)");
    } catch (final Throwable t) {
      throw new RuntimeException("Oops, something unexpected happened: ", t);
    } finally {
      if (shutdown) {
        executorService.shutdown();
      }
    }
  }

  // Use toStringTree()
  @Deprecated
  public String toStringAST() {
    return toStringTree();
  }

  /**
   * Returns a string representation of the parse tree of the parsed input source.
   *
   * @return a string representation of the parse tree of the parsed input source.
   */
  public String toStringTree() {

    final StringBuilder builder = new StringBuilder();

    walk(root, builder);

    return builder.toString();
  }

  /**
   * Walks a (sub) tree of the root of the input source and builds a string representation of the structure of the parse tree.
   * <p/>
   * Note that line breaks and multiple white space characters are trimmed to a single white space character.
   *
   * @param tree
   *          the (sub) tree.
   * @param builder
   *          the StringBuilder to fill.
   */
  private void walk(ParseTree tree, final StringBuilder builder) {

    final List<ParseTree> firstStack = new ArrayList<ParseTree>();
    firstStack.add(tree);

    final List<List<ParseTree>> childListStack = new ArrayList<List<ParseTree>>();
    childListStack.add(firstStack);

    while (!childListStack.isEmpty()) {

      final List<ParseTree> childStack = childListStack.get(childListStack.size() - 1);

      if (childStack.isEmpty()) {
        childListStack.remove(childListStack.size() - 1);
      } else {
        tree = childStack.remove(0);

        String indent = "";

        for (int i = 0; i < childListStack.size() - 1; i++) {
          indent += (childListStack.get(i).size() > 0) ? "|  " : "   ";
        }

        final String tokenName = tree.getClass().getSimpleName().replaceAll("Context$", "");
        final String tokenText = tree.getText().replaceAll("\\s+", " ");

        builder.append(indent)
            .append(childStack.isEmpty() ? "'- " : "|- ")
            .append(tokenName)
            .append(tree.getChildCount() == 0 ? "='" + tokenText + "'" : "")
            .append("\n");

        if (tree.getChildCount() > 0) {
          childListStack.add(new ArrayList<ParseTree>(children(tree)));
        }
      }
    }
  }

  private static List<ParseTree> children(final ParseTree parent) {

    final List<ParseTree> children = new ArrayList<ParseTree>();

    for (int i = 0; i < parent.getChildCount(); i++) {
      children.add(parent.getChild(i));
    }

    return children;
  }

  private void putStringKey(final boolean convertValueToMap, final String key, final Object value, final Map<String, Object> map) {

    if (key == null) {
      throw new RuntimeException("key cannot be null");
    }

    map.put(key, convertValueToMap ? parseSettings.mapper.convertValue(value, Map.class) : value);
  }
}
