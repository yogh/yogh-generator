package liqp.filters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Remove extends Filter {

  /*
   * remove(input, string)
   *
   * remove a substring
   */
  @Override
  public Object apply(final Object value, final Object... params) {
    final Object needle = super.get(0, params);
    if (needle == null) {
      throw new RuntimeException("invalid pattern: " + needle);
    }

    if (super.isArray(value)) {
      final List<Object> lst = new ArrayList<>(Arrays.asList(super.asArray(value)));
      lst.remove(needle);
      return lst.toArray();
    } else {
      final String original = super.asString(value);

      return original.replace(String.valueOf(needle), "");
    }
  }
}
